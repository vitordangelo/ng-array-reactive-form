import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  saleForm: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.saleForm = this.fb.group({
      name_client: ['', Validators.required],
      products: this.fb.array([])
    });
  }

  get productsForm() {
    return this.saleForm.get('products') as FormArray;
  }

  addProduct() {
    const product = this.fb.group({
      price: [],
      quantity: [],
    });

    this.productsForm.push(product);
  }

  deleteProduct(i) {
    this.productsForm.removeAt(i);
  }

  onAddHobby() {
  }

  onSubmit() {
    console.log(this.saleForm.value);
  }
}
